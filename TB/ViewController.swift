//
//  ViewController.swift
//  TB
//
//  Created by 노재형 on 2018. 2. 1..
//  Copyright © 2018년 aSomethings. All rights reserved.
//

import UIKit
import WebKit
import OneSignal

class ViewController: UIViewController,WKNavigationDelegate, WKUIDelegate {
    
    @IBOutlet weak var loadingBar: UIProgressView!
    //@IBOutlet weak var webView: WKWebView!
    @IBOutlet weak var refreshButton: UIBarButtonItem!
    @IBOutlet weak var backButton: UIBarButtonItem!
    @IBOutlet weak  var forwardButton: UIBarButtonItem!
    @IBOutlet weak var homeButton: UIBarButtonItem!
    @IBOutlet weak var settingButton: UIBarButtonItem!
    
    var webView: WKWebView!
    @IBOutlet weak var subView: UIView!
    
    let urlString = "https://tokenbank.co.kr/" //링크
    func webViewInit(){
        let webConfiguration = WKWebViewConfiguration()
        webView = WKWebView(frame: self.subView.frame, configuration: webConfiguration) //WebView 생성

        webView.load(URLRequest(url:URL(string:urlString)!)); //링크 로드
        webView.addObserver(self, forKeyPath: #keyPath(WKWebView.estimatedProgress), options: .new, context: nil)
        webView.allowsBackForwardNavigationGestures = true; //WebView 제스쳐 적용 여부
        webView.isOpaque = false
        self.webView.uiDelegate = self
        
    }
    override func viewDidLoad() {
        super.viewDidLoad()
         webViewInit() //Init 실행
        subView.addSubview(webView) //subView 에 WebView 추가
    }
    /////////// tokenBank SNS 누를시 Safari 로 옮겨가도록 타겟팅
    func webView(_ webView: WKWebView, createWebViewWith configuration: WKWebViewConfiguration, for navigationAction: WKNavigationAction, windowFeatures: WKWindowFeatures) -> WKWebView? {
        if navigationAction.targetFrame == nil {
            UIApplication.shared.open(navigationAction.request.url!, options: [:])
        }
        return nil
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    /////////// webView Observer 값이 변경되면 실행되는 코드
    override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?) {
        
        if keyPath == "estimatedProgress" {
            loadingBar.progress = Float(webView.estimatedProgress)
            backforward()
        }
    }
    ////////////////////////////////////////
    
    /////////// 뒤로가기 버튼 비활성화 && 활성화 코드
    func backforward() {
        if (webView.canGoBack){
            backButton.isEnabled = true
        }else{
            backButton.isEnabled = false
        }
        if (webView.canGoForward){
            forwardButton.isEnabled = true
        }else{
            forwardButton.isEnabled = false
        }
    }
    ////////////////////////////////////////
    
    @IBAction func refreshButtonDidTap() {
      webView.reload()
    }
    @IBAction func backButtonDidTap() {
        
        if (webView.canGoBack == true) {
            webView.goBack()
        }
       
    }
    @IBAction func forwardButtonDidTap() {
        
        if (webView.canGoForward  == true) {
            webView.goForward()
        }
        
    }
    
    @IBAction func homeButtonDidTap(_ sender: Any) {
        let currentURL : String = webView.url!.absoluteString
        if (currentURL != urlString) {
            webView.load(URLRequest(url:URL(string:urlString)!));
        }
    }
    
    @IBAction func settingButtonDidTap(_ sender: Any) {
//        let settingVC = self.storyboard!.instantiateViewController(withIdentifier: "Settings")
//        settingVC.hero.modalAnimationType = .push(direction: HeroDefaultAnimationType.Direction.left)
//        hero.replaceViewController(with: settingVC)
    }
    
    
    
    override func viewDidAppear(_ animated: Bool) {
        
        if (isAppAlreadyLaunchedOnce())
        {
            
        }
        else
        {
           showAdTerms()
        }

    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.setNavigationBarHidden(true, animated: animated)
        var viewBounds:CGRect = self.subView.bounds
        if UIDevice().userInterfaceIdiom == .phone {
            if (UIScreen.main.nativeBounds.height == 2436) {
                viewBounds.origin.y = 44;
                viewBounds.size.height = viewBounds.size.height + 9;
        }
            else{
                viewBounds.origin.y = 20;
                viewBounds.size.height = viewBounds.size.height + 43;
            }
        }
        
        self.webView.frame = viewBounds;
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        self.navigationController?.setNavigationBarHidden(false, animated: animated)
        super.viewWillDisappear(animated)
    }

    
    func showAdTerms() {
        let defaults = UserDefaults.standard
        OneSignal.promptForPushNotifications(userResponse: { accepted in
            print("User accepted notifications: \(accepted)")
        })
        let adAlert = UIAlertController(title: "광고성 정보(PUSH) 수신동의",message: "본 설정은 해당 기기에서만 유효하며 수신에 동의하시면 쿠폰, 할인상품정보 및 주문, 입고알림 등을 받으실 수 있습니다.", preferredStyle: UIAlertControllerStyle.alert)
        
        //UIAlertActionStye.destructive 지정 글꼴 색상 변경
        let cancelButton = UIAlertAction(title: "싫어요", style: UIAlertActionStyle.destructive){ (action: UIAlertAction) in
            print("푸시 알림 해제")
        }
        
        let okAction = UIAlertAction(title: "좋아요", style: UIAlertActionStyle.cancel){ (action: UIAlertAction)
            in
            let formatter = DateFormatter()
            formatter.dateFormat = "yyyy년 MM월 dd일"
            let date = formatter.string(from: Date())
            let message = "토큰뱅크에서 보내는 광고성 알림메시지(PUSH) 수신이 '동의'로 변경되었습니다\n \(date) "
            let confirmAlert = UIAlertController(title: "광고성 정보(PUSH) 수신동의",message:message, preferredStyle: UIAlertControllerStyle.alert)
            let confirmButton = UIAlertAction(title: "확인하였습니다", style: UIAlertActionStyle.default){ (action:UIAlertAction) in
                defaults.set(true, forKey: "marketingPushNotification")
                OneSignal.sendTag("marketing", value: "true")
                defaults.set(date, forKey: "marketingAgreeDate")
            }
                confirmAlert.addAction(confirmButton)
                self.present(confirmAlert,animated: true,completion: nil)
            }
        
        adAlert.addAction(okAction)
        adAlert.addAction(cancelButton)
        
        self.present(adAlert,animated: true,completion: nil)
    }
    func isAppAlreadyLaunchedOnce()->Bool{
        let defaults = UserDefaults.standard
        if let isAppAlreadyLaunchedOnce = defaults.string(forKey: "isAppAlreadyLaunchedOnce"){
            return true
        }else{
            defaults.set(true, forKey: "isAppAlreadyLaunchedOnce")
            return false
        }
    }

}

