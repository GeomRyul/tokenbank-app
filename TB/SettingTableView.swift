//
//  SettingTableView.swift
//  TB
//
//  Created by 노재형 on 2018. 2. 26..
//  Copyright © 2018년 bitbank. All rights reserved.
//

import UIKit
import UserNotifications
import OneSignal

class SettingTableView: UITableViewController {
    @IBOutlet weak var marketingPushNotification: UISwitch!
    @IBOutlet weak var pushNotification: UISwitch!
    @IBOutlet weak var versionLabel: UILabel!
    @IBOutlet weak var marketingPush: UISwitch!
    @IBOutlet weak var appPush: UISwitch!
    @IBOutlet weak var marketingPushCell: UITableViewCell!
    func getVersion() {
        if let version = Bundle.main.infoDictionary?["CFBundleShortVersionString"] as? String {
            self.versionLabel.text = version
        }

    }
    func setSwitches() {
            UNUserNotificationCenter.current().getNotificationSettings { (settings) in
                if settings.authorizationStatus == .authorized {
                        DispatchQueue.main.async {
                            self.appPush.setOn(true, animated: false)
                    }
                }
                else {
                    DispatchQueue.main.async {
                        self.appPush.setOn(false, animated: false)
                    }
                }
            }
        let defaults = UserDefaults.standard
        if (defaults.string(forKey: "marketingPushNotification") != nil){
            DispatchQueue.main.async {
                self.marketingPush.setOn(true, animated: false)
            }
        }else{
            DispatchQueue.main.async {
                self.marketingPush.setOn(false, animated: false)
            }
        }
    }
    
    @IBAction func appPushOnClick(_ sender: UISwitch) {
        if appPush.isOn == true {
            OneSignal.setSubscription(true)
            
        } else {
            OneSignal.setSubscription(false)
            marketingPush.setOn(false,animated: false)
        }
    }
    @IBAction func marketingPushOnClick(_ sender: UISwitch) {
        if marketingPush.isOn == true {
            OneSignal.sendTag("marketing", value: "true")
        } else {
            OneSignal.deleteTag("marketing")
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setSwitches()

        
    }
    override func viewDidLoad() {
        
        super.viewDidLoad()
        getVersion()


        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table view data source

    /*
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "reuseIdentifier", for: indexPath)

        // Configure the cell...

        return cell
    }
    */

    /*
    // Override to support conditional editing of the table view.
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    */

    /*
    // Override to support editing the table view.
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            // Delete the row from the data source
            tableView.deleteRows(at: [indexPath], with: .fade)
        } else if editingStyle == .insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    */

    /*
    // Override to support rearranging the table view.
    override func tableView(_ tableView: UITableView, moveRowAt fromIndexPath: IndexPath, to: IndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the item to be re-orderable.
        return true
    }
    */

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
